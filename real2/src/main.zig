const std = @import("std");

pub fn main() anyerror!void {
    var aim: i32 = 0;
    var depth: i32 = 0;
    var horiz: i32 = 0;

    var file = try std.fs.cwd().openFile("input", .{});
    defer file.close();

    var buf_reader = std.io.bufferedReader(file.reader());
    var in_stream = buf_reader.reader();
    const stdout = std.io.getStdOut().writer();

    var buf: [1024]u8 = undefined;
    while (try in_stream.readUntilDelimiterOrEof(&buf, ' ')) |line| {
        var val: i32 = 0;
        var bf: [1024]u8 = undefined;

        if (try in_stream.readUntilDelimiterOrEof(&bf, '\n')) |str_val| {
            val = try std.fmt.parseInt(i32, str_val, 0);
        }

        if (std.mem.eql(u8, line, "forward")) {
            horiz = horiz + val;
            depth += val * aim;
        } else if (std.mem.eql(u8, line, "up")) {
            aim = aim - val;
        } else if (std.mem.eql(u8, line, "down")) {
            aim = aim + val;
        }
    }

    try stdout.print("D: {d}\nH: {d}\n\tM: {d}", .{depth, horiz, depth * horiz});
}

test "basic test" {
    try std.testing.expectEqual(10, 3 + 7);
}
