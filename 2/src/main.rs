use std::fs;
use std::io::{BufReader, BufRead, Read, Seek, SeekFrom};

fn main() {
    let f = fs::File::open("src/input").unwrap();
    let mut scanner = BufReader::new(f);
    let mut win = 0;
    let mut pr = 0;
    let mut cur = 0;
    let mut total = 0;
    let mut arr: [i32; 3] = [0; 3];

    for (index, line) in scanner.lines().enumerate() {
        let line = line.unwrap();
        cur = line.parse::<i32>().unwrap();

        let i = index % 3;

        pr = win;
        win -= arr[(i) % 3];
        arr[i] = cur;

        win += cur;

        if pr < win && index > 2 {
            total += 1;
        }

    }

    println!("Here: {}", total);
}
