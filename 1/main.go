package main

import "fmt"
import "os"
import "bufio"
import "strconv"

func main() {
  f, _ := os.Open("input");
  defer f.Close();

  scanner := bufio.NewScanner(f);

  i, cur, prev, total := 0, 0, 0, 0;
  for scanner.Scan() {
    cur, _ = strconv.Atoi(scanner.Text());

    if cur > prev && i > 0{
      total ++;
    }

    i ++;
    prev = cur;
  }

  fmt.Printf("%d", total);
}
