package main

import (
  "os"
  "fmt"
  "bufio"
  "math"
  "strconv"
)

func del(arr []string, index int) []string {
  arr[index] = arr[len(arr) - 1];
  arr = arr[:len(arr) - 1];
  return arr;
}

func main() {
  f, _ := os.Open("input");
  defer f.Close();

  scanner := bufio.NewScanner(f);

  var input []string;
  var oxygen, co2 []string;

  for scanner.Scan() {
    i := scanner.Text();
    input = append(input, i);
    oxygen = append(oxygen, i);
    co2 = append(co2, i);
  }

  var gamma, epsilon int;
  length := len(input[0]);

  for j := 0; j < length; j ++ {
    num := 0;

    for _, i := range input {
      if i[j] == '0' {
        num ++;
      }
    }

    if num >= len(input) / 2 {
      epsilon += int(math.Pow(2, float64(length - 1 - j)));
    } else {
      gamma += int(math.Pow(2, float64(length - 1 - j)));
    }
  }

  for len(oxygen) > 1 {
    for j := 0; j < length; j ++ {
      selected := '1';
      num := 0;

      for _, i := range oxygen {
        if i[j] == '0' {
          num ++;
        }
      }

      if num >= len(oxygen) / 2 {
        selected = '0';
        if num == len(oxygen) / 2 {
          selected = '1';
        }
      }

      for i := 0; i < len(oxygen); i ++ {
        if rune(oxygen[i][j]) != selected {
          if len(oxygen) == 1 {
            break;
          }

          oxygen = del(oxygen, i);
          i --;
        }
      }
    }
  }

  for len(co2) > 1 {
    for j := 0; j < length; j ++ {
      selected := '1';
      num := 0;

      for _, i := range co2 {
        if i[j] == '0' {
          num ++;
        }
      }

      if num >= len(co2) / 2 {
        selected = '0';
        if num == len(co2) / 2 {
          selected = '1';
        }
      }

      for i := 0; i < len(co2); i ++ {
        if rune(co2[i][j]) == selected {
          if len(co2) == 1 {
            break;
          }

          co2 = del(co2, i);
          i --;
        }
      }
    }
  }

  ox, _ := strconv.ParseInt(oxygen[0], 2, 64);
  ca, _ := strconv.ParseInt(co2[0], 2, 64);
  fmt.Printf("Part One:\n  %d * %d = %d\n\n", gamma, epsilon, gamma * epsilon);
  fmt.Printf("Part Two:\n  %d * %d = %d\n\n", ox, ca, ox * ca);
}
